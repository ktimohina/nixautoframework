package nix.training.autointensive.spec

import geb.spock.GebReportingSpec
import nix.training.autointensive.page.*

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Blog page"(){

        when:
            to StartPage
        and:
            "User navigates to BlogPage"()
        then:
            at BlogPage
    }
}
