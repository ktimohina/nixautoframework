import org.openqa.selenium.chrome.ChromeDriver

System.setProperty("geb.build.reportsDir","target/geb-reports")
System.setProperty("webdriver.chrome.driver","BrowserDrivers/chromedriver.exe")
System.setProperty("geb.build.baseUrl","https://www.nixsolutions.com")

driver = {
    def driver = new ChromeDriver()
    driver.manage().window().maximize()
    return driver
}
